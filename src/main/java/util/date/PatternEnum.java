package util.date;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2021/1/12 16:07
 */
public enum PatternEnum {
    GENERAL("yyyy-MM-dd HH:mm:ss"),
    DAY("yyyy-MM-dd");


    private String pattern;

    PatternEnum(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
