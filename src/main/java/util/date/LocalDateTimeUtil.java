package util.date;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2021/1/12 16:03
 */
public class LocalDateTimeUtil {
    /**
     * 获取字符串时间
     * @Author Shane
     * @Date 2019/8/2 0:00
     * @param
     * @return
     */
    public static LocalDateTime parse(String local, String pattern) throws Exception{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(local, formatter);
    }

    /**
     * 获取字符串时间
     * @Author Shane
     * @Date 2019/8/2 0:00
     * @param
     * @return
     */
    public static LocalDateTime parse(String local) throws Exception{
        return parse(local, PatternEnum.GENERAL.getPattern());
    }

    /**
     * 格式化时间
     * @Author Shane
     * @Date 2019/9/515:41
     * @param pattern   格式
     * @return
     **/
    public static String format(LocalDateTime local, String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return local.format(formatter);
    }

    /**
     * 格式化时间
     * @Author Shane
     * @Date 2019/9/515:41
     * @param
     * @return
     **/
    public static String format(LocalDateTime local){
        return format(local, PatternEnum.GENERAL.getPattern());
    }

    public static void main(String[] args) {
        LocalDateTime local = LocalDateTime.now();
        System.out.println(format(local));
    }
}
