package util.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 * @Author Shane
 * @Date 2019/8/1 23:48
 */
public class DateUtil {
    public static String getCurrTimeStr(){
        SimpleDateFormat sdf = new SimpleDateFormat(PatternEnum.GENERAL.getPattern());
        return sdf.format(new Date());
    }

    /**
     * 获取当前时间字符串
     * @Author Shane
     * @Date 2019/8/1 23:52
     * @param
     * @return
     */
    public static String getCurrTimeStr(String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(new Date());
    }

    /**
     * 获取当前时间字符串
     * @Author Shane
     * @Date 2019/8/1 23:54
     * @param
     * @return
     */
    public static String parseCurrTime(){
        SimpleDateFormat sdf = new SimpleDateFormat(PatternEnum.GENERAL.getPattern());
        return sdf.format(new Date());
    }

    /**
     * 获取字符串时间
     * @Author Shane
     * @Date 2019/8/2 0:00
     * @param
     * @return
     */
    public static Date parse(String date, String pattern) throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.parse(date);
    }

    /**
     * 获取字符串时间
     * @Author Shane
     * @Date 2019/8/2 0:00
     * @param
     * @return
     */
    public static Date parse(String date) throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat(PatternEnum.GENERAL.getPattern());
        return sdf.parse(date);
    }

    /**
     * 格式化时间
     * @Author Shane
     * @Date 2019/9/515:41
     * @param
     * @return
     **/
    public static String format(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat(PatternEnum.GENERAL.getPattern());
        return sdf.format(date);
    }

    /**
     * 格式化时间
     * @Author Shane
     * @Date 2019/9/515:41
     * @param pattern   格式
     * @return
     **/
    public static String format(Date date, String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * 格式转换
     * @Author Shane
     * @Date 2019/9/312:05
     * @param date  时间字符串
     * @param pattern2  转换后的格式
     * @return
     **/
    public static String transFormatUTC(String date, String pattern2) throws Exception{
        date = date.replace("Z", " UTC");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
        Date d = sdf1.parse(date);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
        return sdf2.format(d);
    }

    /**
     * 获取年龄
     * @Author Shane
     * @Date 2019/8/916:04
     * @param
     * @return
     **/
    public static Integer getAge(Date birthday){
        if(birthday == null){
            return null;
        }
        int age = 0;
        try {
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());// 当前时间

            Calendar birth = Calendar.getInstance();
            birth.setTime(birthday);

            if (birth.after(now)) {//如果传入的时间，在当前时间的后面，返回0岁
                age = 0;
            } else {
                age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
                if (now.get(Calendar.DAY_OF_YEAR) > birth.get(Calendar.DAY_OF_YEAR)) {
                    age += 1;
                }
            }
            return age;
        } catch (Exception e) {//兼容性更强,异常后返回数据
            return 0;
        }
    }
    
    /**
     * 获取近30天的日期
     * @author AppiJ
     * @date 2019年11月25日 15:03:03
     * @param 
     * @return 
     */
    public static String getDayDate(){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -(30-1));
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return format(c.getTime());
    }

    /**
     * 获取近12月的日期
     * @author AppiJ
     * @date 2019年11月25日 15:21:40
     * @param
     * @return
     */
    public static String getYearDate(){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -(12-1));
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return format(c.getTime());
    }

    /**
     * 获取几个月后的日期
     * @Author AppiJ
     * @Date 2020/5/7 18:03
     * @param
     * @return
     */
    public static String getDateByMonth(Integer month){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, month);
        return format(c.getTime(), PatternEnum.DAY.getPattern());
    }

    /**
     * 时间差转换成时分秒
     * @Author Shane
     * @Date 2020/5/19 16:49
     * @param diffSecond    时间差，单位秒
     * @return
     */
    public static String formatTimeDiff(long diffSecond){
        String format = "";
        int day = (int)(diffSecond / (24 * 60 * 60));
        int hour = (int)((diffSecond - day * 24 * 60 * 60)/(60 * 60));
        int minute = (int)((diffSecond - day * 24 * 60 * 60 - hour * 60 * 60) / 60);
        int second = (int)(diffSecond - day * 24 * 60 * 60 - hour * 60 * 60 - minute * 60);

        if(second >= 0){
            format += second+"''";
        }
        if(minute > 0){
            format = minute+"'"+format;
        }
        if(hour > 0){
            format = hour+"º" + format;
        }
        if(day > 0){
            format  = day+" "+format;
        }
        return format;
    }

    /**
     * 获取日期增减多少个小时后的整点时间
     * @Author AppiJ
     * @Date 2020/10/13 17:40
     * @param
     * @return
     */
    public static String getFullTime(Date date, int hour){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return format(c.getTime());
    }

    /**
     * 获取当前天开始时间
     * @return
     * @throws Exception
     */
    public static Date getCurrBeginTime() throws Exception{
        String day = format(new Date(), PatternEnum.DAY.getPattern());
        String date = day + " 00:00:00";

        return parse(date, PatternEnum.GENERAL.getPattern());
    }

    /**
     * 获取当前天结束时间
     * @param
     * @throws Exception
     */
    public static Date getCurrEndTime() throws Exception{
        String day = format(new Date(), PatternEnum.DAY.getPattern());
        String date = day + " 23:59:59";

        return parse(date, PatternEnum.GENERAL.getPattern());
    }

    /**
     * 获取两个日期相差天数
     * @Author AppiJ
     * @Date 2020/11/16 18:58
     * @param
     * @return
     */
    public static long getDateDiffDay(Date beginDate, Date endDate) throws Exception{
        long day = 0;
        beginDate = parse(format(beginDate), PatternEnum.DAY.getPattern());
        endDate = parse(format(endDate), PatternEnum.DAY.getPattern());
        day = (endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
        return day;
    }

    public static void main(String[] args) throws Exception{
//        Date birthday = getDateByStr("2018-08-13", "yyyy-MM-dd");
//        int age = getAge(birthday);
//        System.out.println(age);

        System.out.println(transFormatUTC("2019-09-03T04:16:06Z", "yyyy-MM-dd HH:mm:ss"));
    }
}
