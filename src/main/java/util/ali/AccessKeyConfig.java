package util.ali;

/**
 * 阿里云子账户秘钥
 * @Author Shane
 * @Date 2019/10/4 23:05
 */
public interface AccessKeyConfig {
    String getAccessKeyId();

    String getAccessKeySecret();

}
