package util.ali.sts;

import util.ali.AccessKeyConfig;

public interface STSConfig extends AccessKeyConfig {
    String getEndpoint();

    String getRoleArn();

    String getRoleSessionName();

    String getPolicy();

}
