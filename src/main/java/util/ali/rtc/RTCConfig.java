package util.ali.rtc;

import util.ali.AccessKeyConfig;

public interface RTCConfig extends AccessKeyConfig {
    String getAppId();

    String getAppKey();

    String getGslb();

}
