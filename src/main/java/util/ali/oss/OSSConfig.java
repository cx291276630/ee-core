package util.ali.oss;

import util.ali.AccessKeyConfig;

public interface OSSConfig extends AccessKeyConfig {
    String getEndpoint();

    String getBucketName();

    String getCallback();
}
