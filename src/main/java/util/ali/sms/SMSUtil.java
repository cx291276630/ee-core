package util.ali.sms;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Map;

/**
 * 短信发送工具类
 * @Author Shane
 * @Date 2019/10/4 23:01
 */
public class SMSUtil {
    /**
     * 发送短信
     * @Author Shane
     * @Date 2020/3/21 16:08
     * @param phones    手机号码，多个手机号使用“,”号相连
     * @param templateCode  短信模板id
     * @param params    短信模板变量集合
     * @return
     */
    public static String sendSms(SMSConfig config, String phones, String templateCode, Map<String, String> params) throws Exception{
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", config.getAccessKeyId(), config.getAccessKeySecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");

        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phones);
        request.putQueryParameter("SignName", config.getSignName());
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(params));

        CommonResponse response = client.getCommonResponse(request);
        Map<String, String> result  = (Map<String, String>) JSONObject.parse(response.getData());
        if("OK".equals(result.get("Code"))){
            return null;
        }else if("isv.BUSINESS_LIMIT_CONTROL".equals(result.get("Code"))){
            return "验证码发送太过频繁，请稍后再次尝试";
        }else{
            throw new Exception("验证码发送失败，返回结果：" + response.getData());
        }
    }

    public static void main(String[] args) throws Exception{
        String accessKeyId = "LTAI4Fk1gPgJog66A6Kwb4Qx";
        String accessKeySecret = "Xr6H7NuRPp86hcXiXgSSodZPdPexDH";
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", "13715040291");
        request.putQueryParameter("SignName", "PP交友");
        request.putQueryParameter("TemplateCode", "SMS_174875987");
        request.putQueryParameter("TemplateParam", "{\"code\":\"123456\"}");
            CommonResponse response = client.getCommonResponse(request);

        Map<String, String> result  = (Map<String, String>) JSONObject.parse(response.getData());
        if(!"OK".equals(result.get("Code"))){
            throw new Exception("验证码发送失败，返回结果：" + response.getData());
        }
    }
}
