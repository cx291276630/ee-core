package util.ali.sms;

import util.ali.AccessKeyConfig;

/**
 * @Author Shane
 * @Date 2019/10/4 23:04
 */
public interface SMSConfig extends AccessKeyConfig {
    String getSignName();
}
