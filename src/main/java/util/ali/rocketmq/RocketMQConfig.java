package util.ali.rocketmq;

import util.ali.AccessKeyConfig;

/**
 * @Author Shane
 * @Date 2019/9/19 22:45
 */
public interface RocketMQConfig extends AccessKeyConfig {
    /**
     * RocketMQ 客户端的 GroupID，注意此处的 groupId 和 MQ4IoT 实例中的 GroupId 是2个概念，请按照各自产品的说明申请填写
     */
    String getGroupId();

    /**
     * TCP 接入域名
     */
    String getNamesrvAddr();

    /**
     * MQ4IoT 和 RocketMQ 配合使用时，RocketMQ 客户端仅操作一级 Topic。
     */
    String getTopic();

    String getTag();
}
