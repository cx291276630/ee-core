package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2021/1/8 16:24
 */
public class ValidatorUtil {
    /**
     * 校验手机号
     * @param phone
     * @return
     */
    public static boolean verifyPhone(String phone) {
        if (phone == null) {
            return false;
        } else {
            String regex = "^1\\d{10}$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(phone);
            return m.matches();
        }
    }
}
