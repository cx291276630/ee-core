package util;

/**
 * 分页工具类
 */
public class PageUtil {
    /**
     * 获取分页起始偏移量
     * @param page
     * @param rows
     * @return
     */
    public static Long getOffset(Integer page, Integer rows){
        if(page <= 0){
            return new Long(0);
        }else{
            return new Long(page - 1) * rows;
        }
    }
}
