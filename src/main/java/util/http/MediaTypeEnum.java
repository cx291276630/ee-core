package util.http;

/**
 * 请求媒体类型
 *
 * @author shane
 * @version 1.0.0
 * @Date 2020/12/23 15:55
 */
public enum MediaTypeEnum {
    /**
     * "application/x-www-form-urlencoded"，是默认的MIME内容编码类型，一般可以用于所有的情况，但是在传输比较大的二进制或者文本数据时效率低。
     * 这时候应该使用"multipart/form-data"，如上传文件或者二进制数据和非ASCII数据。
     */
    NORAML("application/x-www-form-urlencoded;charset=utf-8"),
    JSON("application/json;charset=utf-8"),
    TEXT("text/plain;charset=utf-8");

    /**
     * 类型
     */
    private final String type;

    MediaTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
