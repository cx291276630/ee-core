package util.http;

import com.google.gson.Gson;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.StringUtil;
import util.req.RequestUtil;

import java.util.Map;

public class OkHttpUtil {
    private Logger log = LoggerFactory.getLogger(OkHttpUtil.class);

    /**
     * 设置请求头
     * @param headers
     * @return
     * @throws Exception
     */
    public static Request.Builder setHeader(Map<String, Object> headers) throws Exception{
        Request.Builder builder = new Request.Builder();

        if(headers != null){
            for(Map.Entry<String, Object> entry : headers.entrySet()){
                builder.addHeader(entry.getKey(), entry.getValue()+"");
            }
        }

        return builder;
    }

    /**
     * 设置请求
     * @param params
     * @return
     */
    private static RequestBody setBody(Object params, MediaTypeEnum mediaType) throws Exception{
        RequestBody body = null;
        if(StringUtil.isEmpty(mediaType) || MediaTypeEnum.NORAML.equals(mediaType.getType())){  //Params
            body = RequestBody.create(MediaType.parse(mediaType.getType()), RequestUtil.sortParams((Map<String, Object>) params));

        }else if(MediaTypeEnum.JSON.equals(mediaType.getType())){ //Json
            body = RequestBody.create(MediaType.parse(mediaType.getType()), new Gson().toJson(params));

        }else if(MediaTypeEnum.TEXT.equals(mediaType.getType())){ //String
            body = RequestBody.create(MediaType.parse(mediaType.getType()), params+"");
        }

        return body;
    }

    /**
     * get请求
     * @param url   请求地址
     * @return
     * @throws Exception
     */
    public static String get(String url) throws Exception{
        return get(url, null);
    }

    /**
     * get请求
     * @param url   请求地址
     * @param params    请求参数
     * @return
     * @throws Exception
     */
    public static String get(String url, Map<String, Object> params) throws Exception{
        return get(url, params, null);
    }

    /**
     * get请求
     * @param url   请求地址
     * @param params    请求参数
     * @param headers   请求头
     * @return
     * @throws Exception
     */
    public static String get(String url, Map<String, Object> params, Map<String, Object> headers) throws Exception{
        return responseToGet(url, params, headers).body().string();
    }

    /**
     * get请求，返回Response
     * @param url   请求地址
     * @param params    请求参数
     * @param headers   请求头
     * @return
     * @throws Exception
     */
    public static Response responseToGet(String url, Map<String, Object> params, Map<String, Object> headers) throws Exception{
        return response(url, params, headers, null, "GET");
    }

    /**
     * post请求
     * @param url   请求地址
     * @param params    请求参数
     * @return
     * @throws Exception
     */
    public static String post(String url, Map<String, Object> params) throws Exception{
        return post(url, params, null, null);
    }

    /**
     * post请求
     * @param url   请求地址
     * @param params    请求参数
     * @param headers   请求头
     * @return
     * @throws Exception
     */
    public static String post(String url, Map<String, Object> params, Map<String, Object> headers) throws Exception{
        return post(url, params, headers, null);
    }

    /**
     * post请求
     * @param url   请求地址
     * @param params    请求参数
     * @param headers   请求头
     * @param mediaType 媒体类型
     * @return
     * @throws Exception
     */
    public static String post(String url, Map<String, Object> params, Map<String, Object> headers, MediaTypeEnum mediaType) throws Exception{
        return responseToPost(url, params, headers, mediaType).body().string();
    }

    /**
     * post请求，返回Response
     * @param url   请求地址
     * @param params    请求参数
     * @param headers   请求头
     * @param mediaType 媒体类型
     * @return
     * @throws Exception
     */
    public static Response responseToPost(String url, Map<String, Object> params, Map<String, Object> headers, MediaTypeEnum mediaType) throws Exception{
        return response(url, params, headers, mediaType, "POST");
    }

    /**
     * 发送请求
     * @param url   请求地址
     * @param params    请求参数
     * @param headers   请求头
     * @param mediaType 媒体类型
     * @param method    请求类型，GET、POST、DELETE
     * @return
     * @throws Exception
     */
    public static Response response(String url, Map<String, Object> params, Map<String, Object> headers, MediaTypeEnum mediaType, String method) throws Exception{
        OkHttpClient client = new OkHttpClient();

        Request.Builder builder = setHeader(headers);
        RequestBody body = setBody(params, mediaType);

        builder = builder.url(url);
        method = method.toUpperCase();
        if("POST".equals(method)){
            builder = builder.post(body);
        }else if("GET".equals(method)){

        }else if("DELETE".equals(method)){
            builder = builder.delete(body);
        }

        Call call = client.newCall(builder.build());

        return call.execute();
    }

    /**
     * post请求
     * @param url   请求地址
     * @param body   自定义请求体
     * @param headers   请求头
     * @return
     * @throws Exception
     */
    public static Response responseToPost(String url, RequestBody body, Map<String, Object> headers) throws Exception{
        OkHttpClient client = new OkHttpClient();

        Request.Builder builder = setHeader(headers);

        Request request = builder.url(url).post(body).build();
        Call call = client.newCall(request);

        return call.execute();
    }

    public static void main(String[] args) throws Exception{
        String url = "https://www.baidu.com";

        System.out.println(get("http://www.baidu.com"));
        System.out.println(post(url, null));
    }
}
