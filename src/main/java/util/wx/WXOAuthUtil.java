package util.wx;

import com.alibaba.fastjson.JSONObject;
import util.http.OkHttpUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Shane
 * @Date 2020/5/29 21:45
 */
public class WXOAuthUtil {
    static String access_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token";
    static String user_info_url = "https://api.weixin.qq.com/sns/userinfo";
    static String refresh_token_url = "https://api.weixin.qq.com/sns/oauth2/refresh_token";

    /**
     * 通过 code 获取 access_token
     * @Author Shane
     * @Date 2020/5/29 22:36
     * @param 
     * @return
     * {
     *   "access_token": "ACCESS_TOKEN",
     *   "expires_in": 7200,
     *   "refresh_token": "REFRESH_TOKEN",
     *   "openid": "OPENID",
     *   "scope": "SCOPE",
     *   "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
     * }
     */
    public static Map<String, Object> getAccessToken(String code, WXOAuthConfig oauthConfig) throws Exception{
        Map<String, Object> params = new HashMap<>();
        params.put("appid", oauthConfig.getAppid());
        params.put("secret", oauthConfig.getSecret());
        params.put("code", code);
        params.put("grant_type", "authorization_code");

        String result = OkHttpUtil.get(access_token_url, params);
        
        return JSONObject.parseObject(result, Map.class);
    }

    /**
     * 获取用户信息
     * @Author Shane
     * @Date 2020/5/30 0:00
     * @param
     * @return
     * {
     *   "openid": "OPENID",
     *   "nickname": "NICKNAME",
     *   "sex": 1,
     *   "province": "PROVINCE",
     *   "city": "CITY",
     *   "country": "COUNTRY",
     *   "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
     *   "privilege": ["PRIVILEGE1", "PRIVILEGE2"],
     *   "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
     * }
     */
    public static Map<String, Object> getUserInfo(String accessToken, String openId) throws Exception{
        Map<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("openid", openId);

        String result = OkHttpUtil.get(user_info_url, params);

        return JSONObject.parseObject(result, Map.class);
    }

    
    /**
     * 刷新或续期 access_token 使用
     * @Author Shane
     * @Date 2020/5/30 0:11
     * @param 
     * @return
     * {
     *   "access_token": "ACCESS_TOKEN",
     *   "expires_in": 7200,
     *   "refresh_token": "REFRESH_TOKEN",
     *   "openid": "OPENID",
     *   "scope": "SCOPE"
     * }
     * {
     *   "errcode": 40030,
     *   "errmsg": "invalid refresh_token"
     * }
     */
    public static Map<String, Object> refreshToken(String refreshToken, WXOAuthConfig oauthConfig) throws Exception{
        Map<String, Object> params = new HashMap<>();
        params.put("appid", oauthConfig.getAppid());
        params.put("grant_type", "refresh_token");
        params.put("refresh_token", refreshToken);

        String result = OkHttpUtil.get(refresh_token_url, params);

        return JSONObject.parseObject(result, Map.class);
    }

}
