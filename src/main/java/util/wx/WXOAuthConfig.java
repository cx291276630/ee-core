package util.wx;

/**
 * @Author Shane
 * @Date 2020/5/29 21:50
 */
public interface WXOAuthConfig {
    String getAppid();

    String getSecret();
}
