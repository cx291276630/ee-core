package util.netease;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2021/1/13 10:12
 */
public interface App {
    String getAppKey();

    String getAppSecret();
}
