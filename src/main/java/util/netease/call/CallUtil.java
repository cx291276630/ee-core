package util.netease.call;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.netease.App;
import util.netease.Netease;

import java.util.Map;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2021/1/13 11:55
 */
public class CallUtil {
    public static Logger log = LoggerFactory.getLogger(CallUtil.class);

    /**
     * 查看房间信息
     * @param cid   房间 ID
     * @param app
     * @return
     * @throws Exception
     */
    public static Map<String, Object> room(String cid, App app) throws Exception{
        String url = "https://logic-dev.netease.im/v2/api/rooms/" + cid;

        HttpResponse response = HttpRequest.get(url)
                .addHeaders(Netease.getHeaders(app))
                .execute();
        if(response.getStatus() == 200){
            return JSONObject.parseObject(response.body(), Map.class);
        }else{
            log.error("查看房间信息错误，错误状态码" + response.getStatus());
            throw new Exception("查看房间信息错误");
        }
    }

    /**
     * 查看房间内成员信息
     * @param cid   房间 ID
     * @param app
     * @return
     * @throws Exception
     */
    public static Map<String, Object> members(String cid, App app) throws Exception{
        String url = "https://logic-dev.netease.im/v2/api/rooms/" + cid + "/members";

        HttpResponse response = HttpRequest.get(url)
                .addHeaders(Netease.getHeaders(app))
                .execute();
        if(response.getStatus() == 200){
            return JSONObject.parseObject(response.body(), Map.class);
        }else{
            log.error("查看房间内成员信息错误，错误状态码" + response.getStatus());
            throw new Exception("查看房间内成员信息错误");
        }
    }

    /**
     * 移除成员
     * @param cid   房间ID
     * @param uid   用户ID
     * @param app
     * @throws Exception
     */
    public static void kickMember(String cid, String uid, App app) throws Exception{
        String url = "https://logic-dev.netease.im/v2/api/kicklist/" + cid + "/members/" + uid;

        HttpResponse response = HttpRequest.post(url)
                .addHeaders(Netease.getHeaders(app))
                .execute();
        if(response.getStatus() != 200){
            log.error("移除成员错误，错误状态码" + response.getStatus());
            throw new Exception("移除成员错误");
        }
    }

    /**
     * 删除房间
     * @param cid   房间ID
     * @param app
     * @throws Exception
     */
    public static void deletRoom(String cid, App app) throws Exception{
        String url = "https://logic-dev.netease.im/v2/api/rooms/" + cid;

        HttpResponse response = HttpRequest.delete(url)
                .addHeaders(Netease.getHeaders(app))
                .execute();
        if(response.getStatus() != 200){
            log.error("删除房间错误，错误状态码" + response.getStatus());
            throw new Exception("删除房间错误");
        }
    }

    public static void main(String[] args) throws Exception{
        App app = new App() {
            @Override
            public String getAppKey() {
                return "f31308a092ca2a9afaa474af066c1935";
            }

            @Override
            public String getAppSecret() {
                return "d0c8bd355963";
            }
        };

        Map<String, Object> map = room("123", app);
        System.out.println(JSONObject.toJSONString(map));
    }
}
