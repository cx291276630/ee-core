package util.netease.im;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.StringUtil;
import util.netease.App;
import util.netease.Netease;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2021/1/13 10:16
 */
public class IMUtil {
    private static Logger log = LoggerFactory.getLogger(IMUtil.class);

    public static final String create_user_url = "https://api.netease.im/nimserver/user/create.action";
    public static final String update_user_token = "https://api.netease.im/nimserver/user/update.action";
    public static final String get_user_info_url = "https://api.netease.im/nimserver/user/getUinfos.action";
    public static final String update_user_info_url = "https://api.netease.im/nimserver/user/updateUinfo.action";



    /**
     * 创建云通讯id
     * @param accid 云通讯id
     * @param name  昵称
     * @param icon  头像
     * @param token 通讯标识
     * @param app
     * @throws Exception
     */
    public static void createUser(String accid, String name, String icon, String token, App app) throws Exception{
        Map<String, Object> params = new HashMap<>();

        params.put("accid", accid);
        if(StringUtil.isEmpty(name)){
            params.put("name", name);
        }
        if(StringUtil.isEmpty(icon)){
            params.put("icon", icon);
        }
        if(StringUtil.isEmpty(token)){
            params.put("token", token);
        }

        Map<String, Object> map = Netease.post(create_user_url, params, app);
        if(!"200".equals(map.get("code"))){
            log.error("创建网易云通讯ID失败，返回结果：" + JSONObject.toJSONString(map));
            throw new Exception("创建网易云通讯ID失败");
        }
    }

    /**
     * 更新网易云通信token
     * @param accid
     * @param token
     * @param app
     * @throws Exception
     */
    public static void updateUserToken(String accid, String token, App app) throws Exception{
        Map<String, Object> params = new HashMap<>();

        params.put("accid", accid);
        params.put("token", token);

        Map<String, Object> map = Netease.post(update_user_token, params, app);
        if(!"200".equals(map.get("code"))){
            log.error("更新网易云通信tokenID失败，返回结果：" + JSONObject.toJSONString(map));
            throw new Exception("更新网易云通信token失败");
        }
    }

    /**
     * 获取用户名片
     * @param accid 云通讯id
     */
    public static void getUserInfo(String accid, App app) throws Exception{
        Map<String, Object> params = new HashMap<>();

        List<String> userIds = new ArrayList<String>();
        userIds.add(accid);
        params.put("accids", JSONArray.toJSONString(userIds));

        Map<String, Object> map = Netease.post(get_user_info_url, params, app);
        if(!"200".equals(map.get("code"))){
            log.error("获取用户名片失败，返回结果：" + JSONObject.toJSONString(map));
            throw new Exception("获取用户名片失败");
        }
    }

    /**
     * 更新用户名片
     * @param accid
     * @param name  昵称
     * @param icon  头像
     * @param gender    用户性别，0表示未知，1表示男，2女表示女
     */
    public static void updateUserInfo(String accid, String name, String icon, Integer gender, App app) throws Exception{
        Map<String, Object> params = new HashMap<>();

        params.put("accid", accid);
        if(!StringUtil.isEmpty(name)){
            params.put("name", name);
        }
        if(!StringUtil.isEmpty(icon)){
            params.put("icon", icon);
        }
        if(gender != null){
            params.put("gender", gender);
        }

        Map<String, Object> map = Netease.post(update_user_info_url, params, app);
        if(!"200".equals(map.get("code"))){
            log.error("更新用户名片失败，返回结果：" + JSONObject.toJSONString(map));
            throw new Exception("更新用户名片失败");
        }
    }



    public static void main(String[] args) throws Exception{
        App app = new App() {
            @Override
            public String getAppKey() {
                return "f31308a092ca2a9afaa474af066c1935";
            }

            @Override
            public String getAppSecret() {
                return "d0c8bd355963";
            }
        };

        String icon = "https://mm-friend.oss-cn-shenzhen.aliyuncs.com/data/ico/message_icon_service.png";
        createUser("10000", "张三", icon, "123456", app);
    }
}
