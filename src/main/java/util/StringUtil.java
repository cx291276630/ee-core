package util;

/**
 * @author shane
 * @version 1.0.0
 * @Date 2020/12/23 16:15
 */
public class StringUtil {
    private static String[] letter = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "X", "Y", "Z"};

    /**
     * 判空
     * @param o
     * @return
     */
    public static boolean isEmpty(Object o){
        if(o == null || "".equals(o)){
            return true;
        }
        return false;
    }

    /**
     * 获取一个随机数字
     * @return
     */
    public static int getRandomNumber(){
        return (int)(Math.random() * 10);
    }

    /**
     * 获取随机数字字符串
     * @param num
     * @return
     */
    public static String getRandomNumberStr(int num){
        StringBuffer buff = new StringBuffer();
        while (num-- > 0){
            buff.append(getRandomNumber());
        }
        return buff.toString();
    }

    /**
     * 获取6位数字验证码
     * @return
     */
    public static String getRandomNumberStr(){
        return getRandomNumberStr(6);
    }

    /**
     * 获取邀请码，由两位字符四位数字组成
     * @Author Shane
     * @Date 2019/7/1110:23
     * @return
     **/
    public static String getInviteCode(){
        StringBuffer buff = new StringBuffer();
        for(int i=2; i>0; i--){
            buff.append(letter[(int)(Math.random()*letter.length)]);
        }
        buff.append(getRandomNumberStr(4));
        return buff.toString();
    }
}
